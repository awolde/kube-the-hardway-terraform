** Terraform module to install and configure K8s

Motivation - https://github.com/kelseyhightower/kubernetes-the-hard-way

Run this in your cloud shell, meant to work online in Linux for now, not Windows or Mac.
Change the project to your own project!

```
gcloud alpha cloud-shell ssh
git clone repo
cd repo
terraform init
terraform apply
```
