resource "null_resource" "get_deps" {
  provisioner "local-exec" {
    command = "./cfssl.sh"
  }
}

variable "project" {
  default = "hashi-vault-267106"
}

resource "google_compute_network" "network" {
  name = "hard-way-ntk"
  auto_create_subnetworks = false
  project = var.project
}

variable "region" {
  default = "us-central1"
}

resource "google_compute_subnetwork" "subnet" {
  ip_cidr_range = "10.24.0.0/24"
  region = var.region
  name = "hard-way-subnet"
  network = google_compute_network.network.self_link
  project = var.project
}

resource "google_compute_firewall" "fw" {
  name = "allow-internal-nodes"
  network = google_compute_network.network.self_link
  project = var.project
  source_ranges = [google_compute_subnetwork.subnet.ip_cidr_range]
  allow {
    protocol = "tcp"
  }
}

resource "google_compute_firewall" "fw2" {
  name = "allow-ext-nodes"
  network = google_compute_network.network.self_link
  project = var.project
  source_ranges = ["0.0.0.0/0"]
  allow {
    protocol = "tcp"
    ports = [22, 6443]
  }
  allow {
    protocol = "icmp"
  }
}

resource "google_compute_global_address" "api_ip" {
  name = "hard-way-public-ip"
//  region = var.region
//  network = google_compute_network.network.self_link
  project = var.project
}

variable "controllers" {
  default = 3
}
variable "zone" {
  default = "us-central1-a"
}

resource "google_compute_instance" "controllers" {
  count = var.controllers
  zone = var.zone
  project = var.project

//  machine_type = "n1-standard-1"
  machine_type = var.machine_type

  name = "hardway-controller-${count.index}"
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
      size = 200
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet.self_link
//    network_ip = ""
  }
  can_ip_forward = true

  tags = ["kubernetes-the-hard-way","controller"]
  service_account {
    scopes = ["compute-rw","storage-ro","service-management","service-control","logging-write","monitoring"]
  }
  allow_stopping_for_update = true
  metadata_startup_script = data.local_file.bootstrap.content
}

data "local_file" "bootstrap" {
  filename = ("${path.module}/bootstrap.sh")
}

variable "workers" {
  default = 3
}
variable "machine_type" {
  default = "g1-small"
}

resource "google_compute_instance" "workers" {
  count = var.workers
  zone = var.zone
  project = var.project

  //  machine_type = "n1-standard-1"
  machine_type = var.machine_type

  name = "hardway-worker-${count.index}"
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
      size = 200
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet.self_link
    //    network_ip = ""
  }
  can_ip_forward = true
  tags = ["kubernetes-the-hard-way","worker"]
  service_account {
    scopes = ["compute-rw","storage-ro","service-management","service-control","logging-write","monitoring"]
  }
  allow_stopping_for_update = true
  metadata = {
    pod-cidr = "10.200.${count.index}.0/24"
  }
  //  metadata_startup_script = "echo hi > /test.txt"
}

resource "null_resource" "check_nodes" {
  triggers = {
    cluster_instance_ids = "${join(",", google_compute_instance.controllers.*.id)}"
  }
  provisioner "local-exec" {
    command = "./check.sh"
    environment = {
      PROJECT = var.project
      REGION = var.region
      ZONE = var.zone
      WORKERS = replace(join("," , google_compute_instance.workers.*.name),",", " ")
      CONTROLLERS = replace(join("," , google_compute_instance.controllers.*.name),",", " ")
    }
  }
}

resource "null_resource" "get_certs" {
  triggers = {
    cluster_instance_ids = "${join(",", google_compute_instance.controllers.*.id)}"
  }
  provisioner "local-exec" {
    command = "./certs.sh"
    environment = {
      PROJECT = var.project
      REGION = var.region
      ZONE = var.zone
      EXT_IP = google_compute_global_address.api_ip.address
      WRKR_IPS = replace(join("," , google_compute_instance.workers.*.network_interface.0.network_ip),",", " ")
      CTRL_IPS = join("," , google_compute_instance.controllers.*.network_interface.0.network_ip)
      WORKERS = replace(join("," , google_compute_instance.workers.*.name),",", " ")
      CONTROLLERS = replace(join("," , google_compute_instance.controllers.*.name),",", " ")
    }
  }
  depends_on = [null_resource.check_nodes, null_resource.generate_configs]
}

resource "null_resource" "generate_configs" {
  triggers = {
    cluster_instance_ids = "${join(",", google_compute_instance.controllers.*.id)}"
  }
  provisioner "local-exec" {
    command = "./configs.sh"
    environment = {
      PROJECT = var.project
      REGION = var.region
      ZONE = var.zone
      EXT_IP = google_compute_global_address.api_ip.address
      WRKR_IPS = replace(join("," , google_compute_instance.workers.*.network_interface.0.network_ip),",", " ")
      CTRL_IPS = join("," , google_compute_instance.controllers.*.network_interface.0.network_ip)
      WORKERS = replace(join("," , google_compute_instance.workers.*.name),",", " ")
      CONTROLLERS = replace(join("," , google_compute_instance.controllers.*.name),",", " ")
    }
  }
  depends_on = [null_resource.check_nodes]
}

output "api_ip" {
  value = google_compute_global_address.api_ip.address
}
