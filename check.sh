#!/usr/bin/env bash
set -u
gcloud config set project ${PROJECT}
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}

echo "========= Checking if nodes are up !! =============="

for instance in ${WORKERS}; do
  until [[ $(gcloud compute ssh ${instance} -- echo "Hello") ]]
  do
    echo "Sleeping till ${instance} becomes ready...."
    sleep 3
  done
done

for instance in ${CONTROLLERS}; do
  until [[ $(gcloud compute ssh ${instance} -- echo "Hello") ]]
  do
    echo "Sleeping till ${instance} becomes ready...."
    sleep 3
  done
done

echo "========= All nodes up!! ================"
