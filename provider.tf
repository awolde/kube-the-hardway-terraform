terraform {
  backend "gcs" {
    bucket = "mw-acc-tfstate-dev"
    prefix = "terraform/state/k8s-the-hard-way"
  }
}
